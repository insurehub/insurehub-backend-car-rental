package com.kaributechs.carrentals.controllers;

import com.kaributechs.carrentals.models.Car;
import com.kaributechs.carrentals.models.Rental;
import com.kaributechs.carrentals.services.CarService;
import com.kaributechs.carrentals.services.RentalsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/car-rentals")
public class RentalController {
    @Autowired
    private RentalsService service;
    @Autowired
    private CarService carService;

    @PostMapping("/register-car")
    public Car registerACar(@RequestBody Car car){
        return carService.registerCar(car);
    }
    // view all cars
    @GetMapping("/cars")
    public List<Car> viewAllCars(){
        return carService.viewAllCars();
    }

    @PostMapping("/make-reservation")
    public Rental bookARental(@RequestBody Rental rental){
        return service.bookARental(rental);
    }

    @GetMapping
    public List<Rental> viewAllRentals(){
        return service.viewAllRentals();
    }

    @GetMapping("/{rentalId}")
    public Rental viewRentalByID(@PathVariable int rentalId){
        return service.viewRentalById(rentalId);
    }

    @DeleteMapping("/{rentalID}/cancel")
    public String cancelARental(@PathVariable int rentalID){
        return service.cancelARental(rentalID);
    }

}
