package com.kaributechs.carrentals.configurations;


import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.servers.Server;

@OpenAPIDefinition(
        info = @Info(
                title = "Car Rentals",
                description = "" +
                        "This is an api documentation page for Car Rentals developed by Kaributechs",
                contact = @Contact(
                        name = "Wellington Mpofu && Tinashe Makara",
                        email = "wellington.t.mpofu@gmail.com"
                )
        ),
        servers = @Server(url = "http://insurehubbackend.southafricanorth.cloudapp.azure.com:7008/")
)

public class OpenApiConfig {
}
