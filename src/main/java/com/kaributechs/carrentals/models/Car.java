package com.kaributechs.carrentals.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "cars")
public class Car {
    @Id
    private String carRegNumber;
    private String carName;
    private int capacity;
    private String carType;
    private String transmission;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name="rental-id", referencedColumnName="id")
    private Rental rental;
}
