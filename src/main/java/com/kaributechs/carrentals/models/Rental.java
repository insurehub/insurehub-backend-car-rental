package com.kaributechs.carrentals.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "rentals")
public class Rental {
    @Id
    @GeneratedValue
    private int id;
    private LocalDateTime rentalStart;
    private LocalDateTime rentalEnd;
    private String location;
    private String destination;
    private String rentalStatus;

    @OneToOne(mappedBy="rental")
    private Car rentalVehicle;

    @OneToOne(mappedBy ="rental")
    private Customer customer;

    @OneToOne(mappedBy ="rental")
    private Payment payment;
}
