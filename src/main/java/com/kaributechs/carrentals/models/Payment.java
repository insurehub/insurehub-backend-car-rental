package com.kaributechs.carrentals.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "payments")
public class Payment {
    @Id
    @GeneratedValue
    private int id;
    private float paymentAmount;
    private LocalDateTime date; //Review

    @JsonIgnore
    @OneToOne
    @JoinColumn(name="rental-id", referencedColumnName="id")
    private Rental rental;
}
