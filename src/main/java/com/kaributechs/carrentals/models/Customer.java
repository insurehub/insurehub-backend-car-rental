package com.kaributechs.carrentals.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "customers")
public class Customer {
    @Id
    @GeneratedValue
    private int id;
    private String name;
    private String licenseNumber;
    private String phoneNumber;
    private String email;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name="rental-id", referencedColumnName="id")
    private Rental rental;

}
