package com.kaributechs.carrentals.services;

import com.kaributechs.carrentals.models.Payment;
import com.kaributechs.carrentals.models.Rental;
import com.kaributechs.carrentals.repositories.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PaymentService {
    @Autowired
    private PaymentRepository paymentRepo;
    @Autowired
    private RentalsService rentalsService;

    private List<Payment> paymentList = new ArrayList<>();

    // make a payment
    public Payment makePayment(Payment payment){
        paymentList.add(payment);
        return payment;
    }

    // view payment by rental id
    public Payment viewByRentalId(int id){
        Rental rental = rentalsService.viewRentalById(id);
        return rental.getPayment();
    }
}
