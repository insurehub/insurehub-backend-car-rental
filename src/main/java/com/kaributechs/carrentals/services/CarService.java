package com.kaributechs.carrentals.services;

import com.kaributechs.carrentals.models.Car;
import com.kaributechs.carrentals.models.Rental;
import com.kaributechs.carrentals.repositories.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class CarService {

    @Autowired
    private RentalsService rentalsService;
    private List<Car> carList = new ArrayList<>();


    //Register car under rental company
    public Car registerCar(Car car){
        carList.add(car);
        return car;
    }

    //View all cars
    public List<Car> viewAllCars(){
        return carList;
    }


}
