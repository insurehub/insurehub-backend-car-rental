package com.kaributechs.carrentals.services;

import com.kaributechs.carrentals.models.Customer;
import com.kaributechs.carrentals.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerService {
    @Autowired
    private CustomerRepository customerRepo;

    private List<Customer> customerList = new ArrayList<>();

    // submitting customer details
    public Customer addCustomerDetails(Customer customer){
        customerList.add(customer);
        return customer;
    }
}
