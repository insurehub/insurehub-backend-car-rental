package com.kaributechs.carrentals.services;

import com.kaributechs.carrentals.models.Car;
import com.kaributechs.carrentals.models.Customer;
import com.kaributechs.carrentals.models.Payment;
import com.kaributechs.carrentals.models.Rental;
import com.kaributechs.carrentals.repositories.CarRepository;
import com.kaributechs.carrentals.repositories.CustomerRepository;
import com.kaributechs.carrentals.repositories.PaymentRepository;
import com.kaributechs.carrentals.repositories.RentalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class RentalsService {
    @Autowired
    private RentalRepository rentalRepo;
    @Autowired
    private PaymentService paymentService;
    @Autowired CustomerService customerService;

    private List<Rental> rentalList = new ArrayList();

    // book a rental
    public Rental bookARental(Rental rental){
       rentalList.add(rental);
       Customer customer = rental.getCustomer();
       customerService.addCustomerDetails(customer);
       Payment payment = rental.getPayment();
       paymentService.makePayment(payment);
       return rental;

    }

    // view all rentals
    public List<Rental> viewAllRentals(){
        return rentalList;
    }

    // view a rental by id
    public Rental viewRentalById(int id){
        return rentalList.stream().filter(rental -> Objects.equals(rental.getId(), id)).findFirst().get();
    }

    // cancel a rental
    public String cancelARental(int rentalID){
        rentalList.remove(rentalID);
        return "Reservation cancelled";
    }


}
